FROM node:lts-jessie-slim as build
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build

FROM nginx
COPY --from=build /app/build/ /usr/share/nginx/html
COPY ./utils/entrypoint.sh /
COPY ./utils/default.conf /etc/nginx/conf.d/default.conf
CMD ["/entrypoint.sh"]
