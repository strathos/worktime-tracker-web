let url;

if (process.env.NODE_ENV === 'development') {
  url = process.env.REACT_APP_BACKEND_URL || "http://localhost:4000";
} else {
  url = "$REACT_APP_BACKEND_URL";
}

export default {
  url
}
