import { useState, useEffect } from "react";
import Axios from "axios";
import config from "../config";

export const useData = urlParameter => {
  const [fetchedData, setFetchedData] = useState([]);

  useEffect(() => {
    const getOptions = {
      method: "GET",
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
      },
      url: config.url + urlParameter
    };
    Axios(getOptions)
      .then(res => setFetchedData(res.data))
      .catch(err => setFetchedData({ status: err.response.status }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return [fetchedData];
};
