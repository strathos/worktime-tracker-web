import { useEffect, useContext } from "react";
import * as JWT from "jwt-decode";
import moment from "moment";
import { AuthContext } from "../context/auth-context";

export const useLoginCheck = () => {
  const authContext = useContext(AuthContext);

  useEffect(() => {
    if (localStorage.getItem("worktimeToken")) {
      const decodedToken = JWT(localStorage.getItem("worktimeToken"));
      if (decodedToken.exp - moment().unix() > 0) {
        authContext.login({ status: true });
      } else {
        authContext.login({ status: false });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
