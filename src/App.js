import React from "react";
import "./App.css";
import Header from "./components/Header";
import Routes from "./components/Routes";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { indigo, deepOrange, lightBlue, yellow } from "@material-ui/core/colors";
import { BrowserRouter } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import { useLoginCheck } from "./hooks/useLoginCheck";

const theme = createMuiTheme({
  palette: {
    type: "light",
    primary: indigo,
    secondary: deepOrange
  },
  overrides: {
    MuiListItem: {
      root: {
        paddingTop: "15px",
        paddingBottom: "15px"
      }
    }
  }
});
const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
    primary: lightBlue,
    secondary: yellow
  },
  overrides: {
    MuiListItem: {
      root: {
        paddingTop: "15px",
        paddingBottom: "15px"
      }
    }
  }
});
theme.typography.h1 = {
  fontSize: "2.5rem",
  "@media (min-width:600px)": {
    fontSize: "2.5rem"
  },
  [theme.breakpoints.up("md")]: {
    fontSize: "3rem"
  }
};
darkTheme.typography.h1 = {
  fontSize: "2.5rem",
  "@media (min-width:600px)": {
    fontSize: "2.5rem"
  },
  [theme.breakpoints.up("md")]: {
    fontSize: "3rem"
  }
};
darkTheme.typography.body2 = {
  color: "white"
};

const ThemeSelect = () => {
  useLoginCheck();

  const storedTheme = localStorage.getItem("worktimeTheme");
  let selectedTheme = "light";
  if (storedTheme) {
    if (storedTheme === "true") {
      selectedTheme = "dark";
    } else {
      selectedTheme = "light";
    }
  }

  if (selectedTheme === "dark") {
    return (
      <MuiThemeProvider theme={darkTheme}>
        <CssBaseline />
        <BrowserRouter>
          <div className="App">
            <Header />
            <Routes />
          </div>
        </BrowserRouter>
      </MuiThemeProvider>
    );
  } else {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <BrowserRouter>
          <div className="App">
            <Header />
            <Routes />
          </div>
        </BrowserRouter>
      </MuiThemeProvider>
    );
  }
};

function App() {
  return <ThemeSelect />;
}

export default App;
