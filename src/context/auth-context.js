import React, { useState } from "react";

export const AuthContext = React.createContext({
  loggedIn: false,
  loginHandler: () => {}
});

const AuthContextProvider = props => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const loginHandler = props => {
    setIsAuthenticated(props.status);
  };

  return (
    <AuthContext.Provider value={{ login: loginHandler, isAuth: isAuthenticated }}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
