import React, { useContext } from "react";
import { Switch, Route } from "react-router-dom";
import Today from "./Today";
import ThisWeek from "./ThisWeek";
import LastWeek from "./LastWeek";
import Archive from "./Archive";
import Settings from "./Settings";
import User from "./User";
import PrivacyPolicy from "./PrivacyPolicy";
import EditLog from "./EditLog";
import ForgotPassword from "./ForgotPassword";
import ChangePassword from "./ChangePassword";
import { AuthContext } from "../context/auth-context";

const DefaultRoute = () => {
  const authContext = useContext(AuthContext);

  if (authContext.isAuth) {
    return <Route path="/" component={Today} />;
  } else {
    return <Route path="/" component={User} />;
  }
};

const Routes = () => (
  <main>
    <Switch>
      <Route path="/log/today" component={Today} />
      <Route path="/log/weekly-current" component={ThisWeek} />
      <Route path="/log/weekly-previous" component={LastWeek} />
      <Route path="/log/archive" component={Archive} />
      <Route path="/settings" component={Settings} />
      <Route path="/user" component={User} />
      <Route path="/privacy-policy" component={PrivacyPolicy} />
      <Route path="/edit/:handle" component={EditLog} />
      <Route path="/forgot-password" component={ForgotPassword} />
      <Route path="/change-password" component={ChangePassword} />
      <DefaultRoute />
    </Switch>
  </main>
);

export default Routes;
