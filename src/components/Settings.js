import React, { useState, useEffect } from "react";
import * as JWT from "jwt-decode";
import Axios from "axios";
import { Button, Typography, Container, Switch, FormControlLabel } from "@material-ui/core/";
import DeleteIcon from "@material-ui/icons/Delete";
import { Link } from "react-router-dom";
import config from "../config";
import { useLoginCheck } from "../hooks/useLoginCheck";

function Settings() {
  const [pageMessage, setPageMessage] = useState("");
  const [usedTheme, setUsedTheme] = useState(false);
  useLoginCheck();

  const getUserId = () => {
    if (localStorage.getItem("worktimeToken")) {
      const decodedToken = JWT(localStorage.getItem("worktimeToken"));
      return decodedToken.userId;
    }
  };

  const deleteUser = id => {
    const getOptions = {
      method: "DELETE",
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
      },
      url: config.url + "/user/" + id
    };
    Axios(getOptions)
      .then(() => {
        setPageMessage("User deleted");
        localStorage.clear();
      })
      .catch(err => setPageMessage(err.response.status));
  };

  const handleDeleteUser = () => {
    const userId = getUserId();
    if (userId) {
      deleteUser(userId);
    } else {
      setPageMessage("No user id received from storage.");
    }
  };

  useEffect(() => {
    let storedValue = localStorage.getItem("worktimeTheme");
    if (storedValue === "true") {
      setUsedTheme(true);
    }
    if (storedValue === "false") {
      setUsedTheme(false);
    }
  }, []);

  const storageSetter = status => {
    if (status === true) {
      localStorage.setItem("worktimeTheme", true);
    } else {
      localStorage.setItem("worktimeTheme", false);
    }
  };

  const handleChange = name => event => {
    setUsedTheme(event.target.checked);
    storageSetter(event.target.checked);
  };

  return (
    <div className="work-settings">
      <Container fixed>
        <Typography variant="h1" component="h1" gutterBottom>
          Settings
        </Typography>
        <Container className="theme-setting">
          <FormControlLabel
            control={
              <Switch
                checked={usedTheme}
                onChange={handleChange("themeColor")}
                value="themeColor"
                inputProps={{ "aria-label": "primary checkbox" }}
              />
            }
            label="Use the dark theme"
            labelPlacement="top"
          />
        </Container>
        <Container className="container-change-password">
          <Link className="password-link" to="/change-password">
            Change your password
          </Link>
        </Container>
        <Container className="remove-setting">
          <Button color="primary" size="small" className="work-form-remove-user" onClick={handleDeleteUser}>
            Remove your user account <DeleteIcon className="delete-icon" />
          </Button>
        </Container>
        {pageMessage}
      </Container>
    </div>
  );
}

export default Settings;
