import React, { useState, useContext } from "react";
import * as JWT from "jwt-decode";
import moment from "moment";
import { TextField, Button, InputAdornment, Typography, Container } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Lock, Person } from "@material-ui/icons/";
import Axios from "axios";
import config from "../config";
import { AuthContext } from "../context/auth-context";

const Login = () => {
  const [user, setUser] = useState("");
  const [pass, setPass] = useState("");
  const [message, setMessage] = useState("");
  const authContext = useContext(AuthContext);

  const checkLogin = () => {
    if (localStorage.getItem("worktimeToken")) {
      const decodedToken = JWT(localStorage.getItem("worktimeToken"));
      if (decodedToken.exp - moment().unix() > 0) {
        authContext.login({ status: true });
      } else {
        authContext.login({ status: false });
      }
    }
  };

  const handleLogin = async () => {
    const options = {
      method: "POST",
      headers: { "content-type": "application/json" },
      data: {
        email: user,
        password: pass
      },
      url: config.url + "/user/login"
    };
    try {
      const res = await Axios(options);
      await localStorage.setItem("worktimeToken", res.data.token);
      checkLogin();
    } catch (err) {
      console.log(err);
      setMessage("Wrong user email or password.");
    }
  };

  const handleEmailChange = event => {
    setUser(event.target.value);
  };

  const handlePasswordChange = event => {
    setPass(event.target.value);
  };

  return (
    <div className="LoginForm">
      <Container fixed>
        <Typography variant="h1" component="h1" gutterBottom>
          Login
        </Typography>
        <form className="work-form">
          <TextField
            required
            id="standard-name-login"
            label="Email"
            className="textfield"
            margin="normal"
            value={user}
            onChange={handleEmailChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Person />
                </InputAdornment>
              )
            }}
          />
          <TextField
            required
            id="standard-password-input-login"
            label="Password"
            className="password"
            type="password"
            autoComplete="current-password"
            margin="normal"
            value={pass}
            onChange={handlePasswordChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
          />
          <span className="form-message">{message}</span>
          <Button variant="contained" color="primary" className="work-form-submit" onClick={handleLogin}>
            Login
          </Button>
        </form>
      </Container>
      <Container className="container-forgotten-password">
        <Link className="password-link" to="/forgot-password">
          Forgot Password
        </Link>
      </Container>
    </div>
  );
};

export default Login;
