import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";

function PrivacyPolicy() {
  return (
    <div className="privacy">
      <Container fixed>
        <Typography variant="h1" component="h1" gutterBottom>
          Privacy Policy
        </Typography>

        <p>
          We use your data to provide the Service. By using the Service, you agree to the collection and use of
          information in accordance with this policy.
        </p>

        <h2>Information Collection and Use</h2>

        <h3>Types of Data Collected</h3>

        <h4>Personal Data</h4>
        <p>
          We collect your email address to provide you with authentication to the Service. No other personal data is
          collected.
        </p>

        <h4>Tracking / Cookies Data</h4>
        <p>We do not track you or collect any usage data.</p>
        <p>The Service saves your theme selection and authentication token (jwt) to LocalStorage.</p>

        <h2>Use of Data</h2>

        <p>
          The worklog data you provide is used by you and no one else. We do not access your logs unless you ask us to.
        </p>

        <h2>Transfer Of Data</h2>
        <p>
          Your information, including Personal Data, may be transferred to - and maintained on - computers located
          outside of your state, province, country or other governmental jurisdiction where the data protection laws may
          differ than those from your jurisdiction.
        </p>
        <p>
          Your consent to this Privacy Policy followed by your submission of such information represents your agreement
          to that transfer.
        </p>
        <p>
          Worktime tracker will take all steps reasonably necessary to ensure that your data is treated securely and in
          accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization
          or a country unless there are adequate controls in place including the security of your data and other
          personal information.
        </p>

        <h2>Disclosure Of Data</h2>

        <h3>Legal Requirements</h3>
        <p>Worktime tracker will not disclose your Personal Data.</p>

        <h2>Security of Data</h2>
        <p>
          The security of your data is important to us but remember that no method of transmission over the Internet or
          method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect
          your Personal Data, we cannot guarantee its absolute security.
        </p>

        <h2>Children's Privacy</h2>
        <p>Our Service does not address anyone under the age of 18 ("Children").</p>
        <p>
          We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a
          parent or guardian and you are aware that your Child has provided us with Personal Data, please contact us. If
          we become aware that we have collected Personal Data from children without verification of parental consent,
          we take steps to remove that information from our servers.
        </p>

        <h2>Changes to This Privacy Policy</h2>
        <p>
          We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new
          Privacy Policy on this page.
        </p>
        <p>
          We will let you know via email and/or a prominent notice on our Service, prior to the change becoming
          effective and update the "effective date" at the top of this Privacy Policy.
        </p>
        <p>
          You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are
          effective when they are posted on this page.
        </p>

        <h2>Contact Us</h2>
        <p>If you have any questions about this Privacy Policy, please contact us:</p>
        <ul>
          <li>By email: kari.honkasalo@gmail.com</li>
        </ul>
      </Container>
    </div>
  );
}

export default PrivacyPolicy;
