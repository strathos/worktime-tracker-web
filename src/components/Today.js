import React, { useState, useEffect } from "react";
import { Button, Typography, Checkbox, FormControlLabel, Container } from "@material-ui/core";
import Moment from "react-moment";
import useAxios from "axios-hooks";
import { HourCounter } from "./ListHourCounter";
import config from "../config";
import { useLoginCheck } from "../hooks/useLoginCheck";

const Today = () => {
  const [timeIn, setTimeIn] = useState("");
  const [timeOut, setTimeOut] = useState("");
  const [dayId, setDayId] = useState("");
  const [hadLunch, setHadLunch] = useState(true);
  useLoginCheck();

  const [{ data: fetchedData, loading: fetchedDataLoading, error: fetchedDataError }, getDay] = useAxios({
    url: `${config.url}/days/date/today`,
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
    }
  });

  const [{ data: addedData, loading: addedDataLoading, error: addedDataError }, addedDay] = useAxios(
    {
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
      }
    },
    { manual: true }
  );

  const ShowPunchInTime = () => {
    if (timeIn) {
      return (
        <div className="in-time">
          <Typography variant="body1">In</Typography>
          <Moment format="HH:mm:ss">{timeIn}</Moment>
        </div>
      );
    }
    return null;
  };

  const ShowPunchOutTime = () => {
    if (timeOut) {
      return (
        <div className="in-time">
          <Typography variant="body1">Out</Typography>
          <Moment format="HH:mm:ss">{timeOut}</Moment>
        </div>
      );
    }
    return null;
  };

  const handlePunch = dir => {
    if (dayId) {
      addedDay({
        method: "PATCH",
        data: { [dir]: new Date(), break: hadLunch },
        url: `${config.url}/days/${dayId}`
      });
    } else {
      addedDay({
        method: "POST",
        data: { [dir]: new Date(), break: hadLunch },
        url: `${config.url}/days/`
      });
    }
  };

  const handleLunch = () => event => {
    setHadLunch(event.target.checked);
  };

  const WorkTime = () => {
    if (fetchedDataError || fetchedDataLoading || addedDataError || addedDataLoading) {
      return null;
    }
    if (fetchedData.out) {
      return (
        <div>
          <HourCounter item={fetchedData} page={"today"} />
        </div>
      );
    } else {
      if (fetchedData.in) {
        return (
          <div>
            <Typography variant="body1">Time since punch in:</Typography>
            <Moment interval={1000} date={fetchedData.in} durationFromNow />
          </div>
        );
      }
      return null;
    }
  };

  useEffect(() => {
    getDay();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (addedData) {
      getDay();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addedData]);

  useEffect(() => {
    ShowPunchInTime();
    ShowPunchOutTime();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [timeIn, timeOut]);

  useEffect(() => {
    if (fetchedDataError) {
      setTimeIn("");
      setTimeOut("");
      setDayId("");
    }
    if (addedData && addedData._id) {
      setTimeIn(addedData.in);
      setTimeOut(addedData.out);
      setDayId(addedData._id);
      setHadLunch(addedData.break);
    } else if (fetchedData && fetchedData._id) {
      setTimeIn(fetchedData.in);
      setTimeOut(fetchedData.out);
      setDayId(fetchedData._id);
      setHadLunch(fetchedData.break);
    }
  }, [fetchedData, fetchedDataError, addedData]);

  return (
    <div className="work-today">
      <Container fixed>
        <Typography variant="h1" component="h1" gutterBottom>
          Today
        </Typography>
        <Container className="in-container">
          <Button variant="contained" color="primary" onClick={handlePunch.bind(this, "in")}>
            Punch in
          </Button>
        </Container>
        <Container className="out-container">
          <Button variant="contained" color="primary" onClick={handlePunch.bind(this, "out")}>
            Punch out
          </Button>
        </Container>
      </Container>
      <Container className="punched-time-container">
        <ShowPunchInTime />
        <ShowPunchOutTime />
      </Container>
      <Container className="worktime-container">
        <WorkTime />
      </Container>
      <Container className="today-lunch">
        <FormControlLabel
          control={<Checkbox checked={hadLunch} onChange={handleLunch()} value="hadLunch" color="primary" />}
          label="Lunch"
        />
      </Container>
    </div>
  );
};

export default Today;
