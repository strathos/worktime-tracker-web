import React, { useState } from "react";
import { Lock } from "@material-ui/icons/";
import { InputAdornment, Typography, Container, Button, TextField } from "@material-ui/core";
import * as JWT from "jwt-decode";
import moment from "moment";
import Axios from "axios";
import config from "../config";
import { useLoginCheck } from "../hooks/useLoginCheck";

const ChangePassWord = () => {
  const [formPassOld, setFormPassOld] = useState("");
  const [formPass, setFormPass] = useState("");
  const [formPassConfirm, setFormPassConfirm] = useState("");
  const [formMessage, setFormMessage] = useState("");
  const [errorState, setErrorState] = useState(false);

  const handleOldPasswordChange = event => {
    setFormPassOld(event.target.value);
  };

  const handlePasswordChange = event => {
    setFormPass(event.target.value);
  };
  const handleConfirmChange = event => {
    setFormPassConfirm(event.target.value);
  };

  const changePassWord = email => {
    const getOptions = {
      method: "PATCH",
      headers: { "content-type": "application/json" },
      data: {
        email: email,
        password: formPassOld,
        newpassword: formPassConfirm
      },
      url: config.url + "/user/"
    };
    Axios(getOptions)
      .then(() => {
        setFormMessage("Password changed.");
      })
      .catch(setFormMessage("Authorization failed."));
  };

  const checkFields = () => {
    if (formPassOld === "" || formPass === "" || formPassConfirm === "") {
      setFormMessage("Please fill in all the fields.");
    } else {
      checkMatches();
    }
  };

  const checkMatches = () => {
    const passwordMatch = formPass === formPassConfirm;
    if (passwordMatch) {
      if (localStorage.getItem("worktimeToken")) {
        const decodedToken = JWT(localStorage.getItem("worktimeToken"));
        if (decodedToken.exp - moment().unix() > 0) {
          changePassWord(decodedToken.email);
        } else {
          setFormMessage("Not logged in, please login.");
        }
      }
    } else {
      setErrorState(true);
      setFormMessage("Passwords don't match.");
    }
  };

  const handleChangePassWord = () => {
    checkFields();
  };

  useLoginCheck();

  return (
    <div className="ChangePassWordForm">
      <Container fixed>
        <form className="work-form">
          <Typography variant="h1" component="h1" gutterBottom>
            Change password
          </Typography>
          <TextField
            id="standard-password-old"
            label="Old password *"
            className="password"
            type="password"
            margin="normal"
            value={formPassOld}
            onChange={handleOldPasswordChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
          />
          <TextField
            id="standard-password-input"
            label="New password *"
            className="password"
            type="password"
            margin="normal"
            value={formPass}
            onChange={handlePasswordChange}
            error={errorState}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
          />
          <TextField
            id="standard-password-input-verify"
            label="Confirm new password *"
            className="password"
            type="password"
            margin="normal"
            value={formPassConfirm}
            onChange={handleConfirmChange}
            error={errorState}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
          />
          <span className="form-message">{formMessage}</span>
          <Button variant="contained" color="primary" className="work-form-submit" onClick={handleChangePassWord}>
            Change Password
          </Button>
        </form>
      </Container>
    </div>
  );
};

export default ChangePassWord;
