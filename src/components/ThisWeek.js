import React from "react";
import WorkList from "./WorkList";
import { useLoginCheck } from "../hooks/useLoginCheck";

const ThisWeek = () => {
  useLoginCheck();
  return <WorkList parameter="/days/week/this" title="This Week" />;
};

export default ThisWeek;
