import React from "react";
import Moment from "react-moment";

export const HourCounter = item => {
  if (item.page === "list" && !item.item.out) {
    return null;
  }

  if (item.item.break) {
    return (
      <div className="total-hours">
        <strong>Total hours (with lunch):</strong>
        <br />
        <Moment subtract={{ minutes: 30 }} duration={item.item.in} date={item.item.out} />
      </div>
    );
  }
  return (
    <div className="total-hours">
      <strong>Total hours (no lunch):</strong>
      <br />
      <Moment duration={item.item.in} date={item.item.out} />
    </div>
  );
};
