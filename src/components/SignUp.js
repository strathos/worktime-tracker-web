import React, { useState } from "react";
import { Lock, Person } from "@material-ui/icons/";
import {
  InputAdornment,
  Typography,
  Container,
  Button,
  TextField,
  Checkbox,
  FormControlLabel
} from "@material-ui/core";
import Axios from "axios";
import config from "../config";

const SignUp = () => {
  const [formUser, setFormUser] = useState("");
  const [formPass, setFormPass] = useState("");
  const [privacy, setPrivacy] = useState(false);
  const [formPassConfirm, setFormPassConfirm] = useState("");
  const [formMessage, setFormMessage] = useState("");
  const [errorState, setErrorState] = useState(false);

  const handleEmailChange = event => {
    setFormUser(event.target.value);
  };
  const handlePasswordChange = event => {
    setFormPass(event.target.value);
  };
  const handleConfirmChange = event => {
    setFormPassConfirm(event.target.value);
  };

  const createUser = () => {
    const getOptions = {
      method: "POST",
      headers: { "content-type": "application/json" },
      data: {
        email: formUser,
        password: formPass
      },
      url: config.url + "/user/signup"
    };
    Axios(getOptions)
      .then(() => {
        setFormMessage("User created.");
      })
      .catch(err => setFormMessage(err.response.message));
  };

  const checkFields = () => {
    if (formUser === "" || formPass === "" || formPassConfirm === "") {
      setFormMessage("Please fill in all the fields.");
    } else {
      checkMatches();
    }
  };

  const checkMatches = () => {
    const passwordMatch = formPass === formPassConfirm;
    if (passwordMatch) {
      createUser();
    } else {
      setErrorState(true);
      setFormMessage("Passwords don't match.");
    }
  };

  const handleSignUp = () => {
    if (privacy) {
      checkFields();
    } else {
      setFormMessage("You haven't read the Privacy Policy");
    }
  };

  const handlePrivacy = () => event => {
    setPrivacy(event.target.checked);
  };

  const label = (
    <span>
      I have read the&nbsp;
      <a href="/privacy-policy" target="_blank" className="text-link">
        Privacy Policy
      </a>
    </span>
  );

  return (
    <div className="SignUpForm">
      <Container fixed>
        <Typography variant="h1" component="h1" gutterBottom>
          Sign Up
        </Typography>
        <form className="work-form">
          <TextField
            id="standard-name"
            label="User email"
            className="textfield"
            margin="normal"
            value={formUser}
            onChange={handleEmailChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Person />
                </InputAdornment>
              )
            }}
          />
          <TextField
            id="standard-password-input"
            label="Password"
            className="password"
            type="password"
            margin="normal"
            value={formPass}
            onChange={handlePasswordChange}
            error={errorState}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
          />
          <TextField
            id="standard-password-input-verify"
            label="Confirm password"
            className="password"
            type="password"
            margin="normal"
            value={formPassConfirm}
            onChange={handleConfirmChange}
            error={errorState}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock />
                </InputAdornment>
              )
            }}
          />
          <span className="form-message">{formMessage}</span>

          <FormControlLabel
            control={<Checkbox checked={privacy} onChange={handlePrivacy()} value="readPrivacy" color="primary" />}
            label={label}
          />
          <Button variant="contained" color="primary" className="work-form-submit" onClick={handleSignUp}>
            Sign Up
          </Button>
        </form>
      </Container>
    </div>
  );
};

export default SignUp;
