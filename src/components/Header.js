import React, { useContext } from "react";
import {
  AppBar,
  Toolbar,
  Container,
  SwipeableDrawer,
  Button,
  List,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { AccessTime, History, Settings, Person, Subject } from "@material-ui/icons/";
import ListIcon from "@material-ui/icons/List";
import { Link } from "react-router-dom";
import { AuthContext } from "../context/auth-context";
import { useLoginCheck } from "../hooks/useLoginCheck";

const useStyles = makeStyles({
  list: {
    width: 250
  }
});

const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

function Header() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });

  useLoginCheck();
  const authContext = useContext(AuthContext);

  const handleLogOut = () => {
    localStorage.removeItem("worktimeToken");
    authContext.login({ status: false });
  };

  const LoggedHeader = () => {
    if (authContext.isAuth) {
      return (
        <Button variant="contained" color="secondary" onClick={handleLogOut}>
          Log out
        </Button>
      );
    }
    return <Typography variant="body2">Not logged in</Typography>;
  };

  const toggleDrawer = (side, open) => event => {
    if (event && event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        <ListItem button key="today" component={Link} to="/log/today">
          <ListItemIcon>
            <AccessTime />
          </ListItemIcon>
          <ListItemText primary="Today" />
        </ListItem>
        <ListItem button key="thisweek" component={Link} to="/log/weekly-current">
          <ListItemIcon>
            <History />
          </ListItemIcon>
          <ListItemText primary="This week" />
        </ListItem>
        <ListItem button key="lastweek" component={Link} to="/log/weekly-previous">
          <ListItemIcon>
            <History />
          </ListItemIcon>
          <ListItemText primary="Last week" />
        </ListItem>
        <ListItem button key="archive" component={Link} to="/log/archive">
          <ListItemIcon>
            <ListIcon />
          </ListItemIcon>
          <ListItemText primary="Archive" />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem button key="settings" component={Link} to="/settings">
          <ListItemIcon>
            <Settings />
          </ListItemIcon>
          <ListItemText primary="Settings" />
        </ListItem>
        <ListItem button key="user" component={Link} to="/user">
          <ListItemIcon>
            <Person />
          </ListItemIcon>
          <ListItemText primary="User" />
        </ListItem>
        <ListItem button key="privacy" component={Link} to="/privacy-policy">
          <ListItemIcon>
            <Subject />
          </ListItemIcon>
          <ListItemText primary="Privacy policy" />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div className="app-header">
      <AppBar position="static" style={{ background: "rgba(0,0,0,0.5)" }}>
        <Container fixed>
          <Toolbar className="header-bar">
            <Button variant="contained" color="primary" onClick={toggleDrawer("left", true)}>
              Menu
            </Button>
            <LoggedHeader />
            <SwipeableDrawer
              open={state.left}
              onClose={toggleDrawer("left", false)}
              onOpen={toggleDrawer("left", true)}
              disableBackdropTransition={!iOS}
              disableDiscovery={iOS}
            >
              {sideList("left")}
            </SwipeableDrawer>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}

export default Header;
