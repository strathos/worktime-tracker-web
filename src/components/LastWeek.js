import React from "react";
import WorkList from "./WorkList";
import { useLoginCheck } from "../hooks/useLoginCheck";

const LastWeek = () => {
  useLoginCheck();
  return <WorkList parameter="/days/week/last" title="Last Week" />;
};

export default LastWeek;
