import React, { useEffect } from "react";
import Moment from "react-moment";
import useAxios from "axios-hooks";
import { Typography, Container, List, ListItem, IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { Link } from "react-router-dom";
import { HourCounter } from "./ListHourCounter";
import config from "../config";

const WorkList = props => {
  const [{ data: fetchedData, loading: fetchedDataLoading, error: fetchedDataError }, getWeek] = useAxios({
    url: `${config.url}${props.parameter}`,
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
    }
  });

  const [{ data: deletedData }, deleteDay] = useAxios({
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
    }
  });

  const handleDelete = id => {
    deleteDay({
      url: `${config.url}/days/${id}`
    });
  };

  const PunchOutPrint = item => {
    if (item.item.out) {
      return (
        <div className="punch-out-time">
          <strong>Punched out:</strong>
          <br /> <Moment format="LLLL">{item.item.out}</Moment>
        </div>
      );
    }
    return null;
  };

  useEffect(() => {
    getWeek();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (deletedData) {
      getWeek();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [deletedData]);

  if (fetchedDataLoading) return <p>Loading...</p>;
  if (fetchedDataError) return <p>Error loading the data</p>;
  if (fetchedData.length > 0) {
    return (
      <div className="work-log">
        <Container fixed>
          <Typography variant="h1" component="h1" gutterBottom>
            {props.title}
          </Typography>
          <div className="log-list">
            <List>
              {fetchedData.map(item => {
                return (
                  <ListItem divider className="log-item" key={item._id}>
                    <div className="log-item-data">
                      <div className="punch-in-time">
                        <strong>Punched in:</strong>
                        <br /> <Moment format="LLLL">{item.in}</Moment>
                      </div>
                      <PunchOutPrint item={item} />
                      <HourCounter item={item} page={"list"} />
                    </div>
                    <div className="list-button-container">
                      <Link className="edit-button" to={`/edit/${item._id}`}>
                        Edit
                      </Link>
                      <IconButton
                        className="delete-button"
                        aria-label="delete"
                        onClick={handleDelete.bind(this, item._id)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </div>
                  </ListItem>
                );
              })}
            </List>
          </div>
        </Container>
      </div>
    );
  } else {
    return (
      <Container fixed>
        <Typography variant="h1" component="h1" gutterBottom>
          {props.title}
        </Typography>
        No data.
      </Container>
    );
  }
};

export default WorkList;
