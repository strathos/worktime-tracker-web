import React, { useState } from "react";
import { TextField, Button, Typography, InputAdornment, Container } from "@material-ui/core";
import { Email } from "@material-ui/icons/";
import Axios from "axios";
import config from "../config";

const ForgotPassword = () => {
  const [user, setUser] = useState("");
  const [message, setMessage] = useState("");

  const handleEmailChange = event => {
    setUser(event.target.value);
  };

  const handleForgottenPassword = async () => {
    const options = {
      method: "POST",
      headers: { "content-type": "application/json" },
      data: {
        email: user
      },
      url: config.url + "/user/forgot"
    };
    try {
      const res = await Axios(options);
      console.log(res);
      setMessage("If the email exists, an email has been sent.");
    } catch (err) {
      console.log(err);
      setMessage("There was an error.");
    }
  };

  return (
    <div className="ForgotPasswordForm">
      <Container fixed>
        <Typography variant="h1" component="h1" gutterBottom>
          Forgot your password?
        </Typography>
        <form className="work-form">
          <TextField
            required
            id="standard-name-forgot"
            label="Email"
            className="textfield"
            margin="normal"
            value={user}
            onChange={handleEmailChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Email />
                </InputAdornment>
              )
            }}
          />
          <span className="form-message">{message}</span>
          <Button variant="contained" color="primary" className="work-form-submit" onClick={handleForgottenPassword}>
            Reset password
          </Button>
        </form>
      </Container>
    </div>
  );
};

export default ForgotPassword;
