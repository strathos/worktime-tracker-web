import React, { useState, useEffect } from "react";
import { Button, Typography, Checkbox, FormControlLabel, Container } from "@material-ui/core";
import { MuiPickersUtilsProvider, KeyboardTimePicker, KeyboardDatePicker } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import useAxios from "axios-hooks";
import config from "../config";

const EditLog = props => {
  const dayId = props.match.params.handle;
  const [dateIn, setDateIn] = useState("");
  const [dateOut, setDateOut] = useState("");
  const [hadLunch, setHadLunch] = useState(true);
  const [punchIn, setPunchIn] = useState(true);
  const [punchOut, setPunchOut] = useState(true);

  const [{ data: fetchedData, loading: fetchedDataLoading, error: fetchedDataError }, getDay] = useAxios({
    url: `${config.url}/days/id/${dayId}`,
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
    }
  });

  const [{ data: addedData }, addedDay] = useAxios(
    {
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("worktimeToken")}`
      }
    },
    { manual: true }
  );

  useEffect(() => {
    getDay();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (addedData) {
      getDay();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addedData]);

  useEffect(() => {
    if (fetchedDataError) {
      setDateIn(fetchedData.in);
      setDateOut(fetchedData.out || new Date());
      setHadLunch(fetchedData.break || true);
    }
    if (addedData && addedData._id) {
      setDateIn(addedData.in);
      setDateOut(addedData.out);
      setHadLunch(addedData.break);
    } else if (fetchedData && fetchedData._id) {
      setDateIn(fetchedData.in);
      setDateOut(fetchedData.out);
      setHadLunch(fetchedData.break);
    }
  }, [fetchedData, fetchedDataError, addedData]);

  const handleDateInChange = date => {
    setDateIn(date.toISOString());
  };

  const handleDateOutChange = date => {
    setDateOut(date.toISOString());
  };

  const handleIn = name => event => {
    setPunchIn(event.target.checked);
  };

  const handleOut = name => event => {
    setPunchOut(event.target.checked);
  };

  const handleLunch = name => event => {
    setHadLunch(event.target.checked);
  };

  const handleUpdate = () => {
    let data = { break: hadLunch };

    if (punchIn) {
      data = { ...data, in: dateIn };
    }
    if (punchOut) {
      data = { ...data, out: dateOut };
    }

    addedDay({
      method: "PATCH",
      data,
      url: `${config.url}/days/${dayId}`
    });
  };

  if (fetchedDataLoading) return <p>Loading...</p>;
  if (fetchedDataError) return <p>Error loading the data</p>;
  if (dateIn || dateOut) {
    return (
      <div className="work-edit">
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <Container className="today-in">
            <Container className="check-in">
              <FormControlLabel
                control={<Checkbox checked={punchIn} onChange={handleIn("punchIn")} value="punchIn" color="primary" />}
              />
              <Typography variant="h3" component="h3">
                In
              </Typography>
            </Container>

            <KeyboardDatePicker
              margin="normal"
              id="mui-pickers-date-in"
              label="Date"
              value={dateIn}
              onChange={handleDateInChange}
              KeyboardButtonProps={{
                "aria-label": "change date"
              }}
            />
            <KeyboardTimePicker
              margin="normal"
              id="mui-pickers-time-in"
              label="Time"
              value={dateIn}
              onChange={handleDateInChange}
              KeyboardButtonProps={{
                "aria-label": "change time"
              }}
            />
          </Container>
          <Container className="today-out">
            <Container className="check-out">
              <FormControlLabel
                control={
                  <Checkbox checked={punchOut} onChange={handleOut("punchOut")} value="punchOut" color="primary" />
                }
              />
              <Typography variant="h3" component="h3">
                Out
              </Typography>
            </Container>
            <KeyboardDatePicker
              margin="normal"
              id="mui-pickers-date-out"
              label="Date"
              value={dateOut}
              onChange={handleDateOutChange}
              KeyboardButtonProps={{
                "aria-label": "change date"
              }}
            />
            <KeyboardTimePicker
              margin="normal"
              id="mui-pickers-time-out"
              label="Tie"
              value={dateOut}
              onChange={handleDateOutChange}
              KeyboardButtonProps={{
                "aria-label": "change time"
              }}
            />
          </Container>
        </MuiPickersUtilsProvider>
        <Container className="today-lunch">
          <FormControlLabel
            control={
              <Checkbox checked={hadLunch} onChange={handleLunch("hadLunch")} value="hadLunch" color="primary" />
            }
            label="Lunch"
          />
        </Container>
        <Container>
          <Button variant="contained" color="secondary" className="today-update" onClick={handleUpdate}>
            Update
          </Button>
        </Container>
      </div>
    );
  } else {
    return null;
  }
};

export default EditLog;
