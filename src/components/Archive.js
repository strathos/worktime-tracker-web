import React from "react";
import WorkList from "./WorkList";
import { useLoginCheck } from "../hooks/useLoginCheck";

const Archive = () => {
  useLoginCheck();
  return <WorkList parameter="/days/" title="Archive" />;
};

export default Archive;
