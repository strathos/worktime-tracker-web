#!/bin/sh

# Replace env vars in JavaScript files
echo "Replacing env vars in static/js/"
for file in /usr/share/nginx/html/static/js/main*.js;
do
    echo "Processing $file ...";

    # Use the existing JS file as template
    if [ ! -f $file.tmpl.js ]; then
        cp $file $file.tmpl.js
    fi

    envsubst '$REACT_APP_BACKEND_URL' < $file.tmpl.js > $file
done

echo "Starting Nginx"
nginx -g 'daemon off;'
